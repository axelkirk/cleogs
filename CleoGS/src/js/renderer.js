//lib import
// const sudo = require('sudo-prompt');
// const jsPDF = require('jspdf');
const fs = require("fs");
const Swal = require('sweetalert2')
const remote = require("electron").remote;
const jsPDF = require('jspdf');


var app = {
    init: function () {
        //var declaration
        app.counterCanvas = 0;
        app.appJSONDescriber = {};
        app.appJSONDescriber['card'] = {}
        app.appContainer = $('#appContainer');
        app.color = {
            'name': '#4169E1',
            'things': '#03224c',
            'adjectif': '#03224c',
            'animal': '#87CEEB',
            'toolWord': '#FFFF00',
            'verbe': '#FF0000'
        }

        app.launchCanvasCreation = 0;
        // load home page
        app.appContainer.load('./html/homePage/homePage.html', () => { app.addListenerToHomePage(false) });
    },
    /////////////////// Page Home 
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    addListenerToHomePage: function (notFirstTime) {
        app.homeFill = 1;
        console.log("addListenerToHomePage");
        if (notFirstTime) {
            app.homeFill--;
            var oldCount = parseInt(app.appJSONDescriber['cardQuantity'])
            $('#choiceCardQuantity').val(oldCount)
            for (let i = 1; i < oldCount * 1 + 1; i++) {
                // check if this key already exist
                if (!Object.keys(app.appJSONDescriber['card']).includes(`${i}`)) {
                    app.appJSONDescriber['card'][i] = {};
                }
                var card = $('<li>');
                card.addClass(`card card-${i}`)
                card.text(i)
                card.load('./html/homePage/cardLi.html', () => { app.addListenerToCardListFromHomePage(i) })
                card.appendTo($(".cardDetail"));
            }
        }
        // link to the number input - choose quantity different card
        $('#choiceCardQuantity').on('click', (e) => {
            app.homeFill=0;
            if(e.target.value==0){
                app.homeFill=1;
            }
            //clean the list each time we add a number of card
            app.cleanList('cardDetail');
            if (app.appJSONDescriber['cardQuantity'] > e.target.value) {

                delete app.appJSONDescriber['card'][app.appJSONDescriber['cardQuantity']];
            }

            var count = e.target.value;
            app.appJSONDescriber['cardQuantity'] = count;

            // loop through the new quantity of card to create the HTML
            // load card detail list HTML
            for (let i = 1; i < count * 1 + 1; i++) {
                app.homeFill++;
                console.log(app.homeFill)
                // check if this key already exist
                if (!Object.keys(app.appJSONDescriber['card']).includes(`${i}`)) {
                    app.appJSONDescriber['card'][i] = {};
                }
                var card = $('<li>');
                card.addClass(`card card-${i}`)
                card.text(i)
                card.load('./html/homePage/cardLi.html', () => { app.addListenerToCardListFromHomePage(i) })
                card.appendTo($(".cardDetail"));

            }
            console.log(app.appJSONDescriber['card'])
        })



        // switch to second page and load the image choice page
        $('#validerHomePage').on('click', () => {
            console.log(app.homeFill)
            if (app.homeFill <= 0) {
                app.cleanList('appContainer');
                app.appContainer.load('./html/ImageChoicePage/imageChoice.html', app.loadImageChoicePageHTML)
            } else {
                Swal.fire({
                    title: 'Error!',
                    text: 'Merci de remplir tous les champs',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            }
        })

    },

    // add listenner to different input on the home page
    addListenerToCardListFromHomePage: function (i) {
        var cardObj = app.appJSONDescriber.card[i];
        var cardList = app.appJSONDescriber.card;

        var motRappelDiv = $(`.card-${i}`).children('.motRappelDiv');
        var adjectifDiv = $(`.card-${i}`).children('.adjectif');
        var rectoVersoDiv = $(`.card-${i}`).children('.rectoVerso');

        adjectifDiv.children('input').attr('name', `adjectif-${i}`);
        rectoVersoDiv.children('input').attr('name', `rectoVerso-${i}`);

        // check if it already exist a mot rappel
        if (undefined != cardObj['motRappel']) {

            motRappelDiv.children('input').val(cardObj['motRappel'])
            rectoVersoDiv.show()

            // check if adjectif coice already check
            if (undefined != cardObj['rectoVerso']) {

                rectoVersoDiv.children('.rectoVerso-input-yes').prop('checked', cardObj['rectoVerso'])
                rectoVersoDiv.children('.rectoVerso-input-no').prop('checked', !cardObj['rectoVerso'])
                adjectifDiv.show()

                //check if rectoVerso choice already check
                if (undefined != cardObj['adjectif']) {
                    adjectifDiv.children('.adjectif-input-yes').prop('checked', cardObj['adjectif'])
                    adjectifDiv.children('.adjectif-input-no').prop('checked', !cardObj['adjectif'])
                }
            }
        }
        // add unique class
        motRappelDiv.children('input').addClass(`motRappel-input-${i}`);

        // add listener to show the adjectif input

        motRappelDiv.children('input').on('focusin', function(){
            console.log("Saving value " + $(this).val());
            $(this).data('val', $(this).val());
        });
        
        motRappelDiv.children('input').on('keyup', (e) => {
            cardObj['motRappel'] = e.target.value;
            rectoVersoDiv.show()
            if($(this).data('val')==""){
                app.homeFill--;
            }
            if(e.target.value=="" && app.homeFill<=0){app.homeFill=1;}
        })

        rectoVersoDiv.children('.rectoVerso-input-yes').on('click', (e) => {
            // create another line in the array for the verso 
            cardList[`${i}-verso`] = {
                'motRappel': cardObj['motRappel'],
                'rectoVerso': true
            };
            cardObj['rectoVerso'] = true;
            adjectifDiv.show()
        })

        rectoVersoDiv.children('.rectoVerso-input-no').on('click', (e) => {
            // if already click once and de-click, delete the key verso
            if (undefined != cardList[`${i}-verso`]) {
                delete cardList[`${i}-verso`]
            }
            cardObj['rectoVerso'] = false;
            adjectifDiv.show()
        })

        adjectifDiv.children('.adjectif-input-yes').on('click', (e) => {
            // create another line in the array for the verso 
            adjectifDiv.children('.adjectif-input-no').prop('checked', false);
            cardList[`${i}-feminin`] = {
                'motRappel': cardObj['motRappel'],
                'adjectif': true,
                'rectoVerso': cardObj['rectoVerso']
            }
            if (undefined != cardList[`${i}-verso`]) {
                cardList[`${i}-verso`]['adjectif'] = true;
            }
            if (cardObj['rectoVerso']) {
                cardList[`${i}-verso-feminin`] = {
                    'motRappel': cardObj['motRappel'],
                    'rectoVerso': true,
                    'adjectif': true
                };
            }
            cardObj['adjectif'] = true;
            app.homeFill--;
        })

        adjectifDiv.children('.adjectif-input-no').on('click', (e) => {
            if (undefined != cardList[`${i}-feminin`]) {
                delete cardList[`${i}-feminin`]
            }
            if (undefined != cardList[`${i}-verso-feminin`]) {
                delete cardList[`${i}-verso-feminin`]
            }
            cardObj['adjectif'] = false;
            app.homeFill--;
        })
    },

    /////////////////// Page Image Selection 
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    // generate HTML base on different card quantity
    loadImageChoicePageHTML: function (notFirstTime) {
        console.log("loadImageChoicePageHTML");
        app.imageFill=0;
        app.cleanList('cardImageList');

        Object.keys(app.appJSONDescriber.card).sort((a, b) => (a > b) ? 1 : -1).forEach((cardKey) => { app.addListenerToImageChoicePageHTML(cardKey) })

        var buttonLoadTextChoicePage = $('.loadTextChoicePage');
        buttonLoadTextChoicePage.on('click', () => {
            console.log(app.imageFill)
            if(app.imageFill <= 0){
                app.cleanList('appContainer');
                app.appContainer.load('./html/TextChoicePage/textChoice.html', app.loadTextChoicePageHTML)
            }else{
                Swal.fire({
                    title: 'Error!',
                    text: 'Merci de remplir tous les champs',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            }
        })
        var buttonLoadHomePage = $('.loadHomePage');
        buttonLoadHomePage.on('click', () => {
            app.homeFill=0
            app.cleanList('appContainer');
            app.appContainer.load('./html/homePage/homePage.html', () => { app.addListenerToHomePage(true) });
        })
    },

    // add listener and unique class to each input - also get value from JSON
    addListenerToImageChoicePageHTML: function (cardKey) {
        console.log("addListenerToImageChoicePageHTML:", cardKey);
        app.imageFill++
        const { card } = app.appJSONDescriber;
        var ul = $('.cardImageList');

        var li = $('<li>');
        li.addClass(`card cardImageLi cardImageLi-${cardKey}`)
        li.appendTo(ul);

        li.load('./html/ImageChoicePage/imageCardLi.html', () => {

            li.children('.motRappel').text(card[cardKey]['motRappel'])

            if (undefined != card[cardKey]['rectoVerso']) {
                if (card[cardKey]['rectoVerso']) {
                    if (cardKey.length > 2) {
                        array = cardKey.split("-")
                        if (array.length == 3) {
                            li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin:  verso et feminin  `);
                        } else if (array.length == 2) {
                            if (array[1] == "feminin") {
                                li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin: recto et feminin`);
                            } else if (array[1] == "verso") {
                                if (card[cardKey]['adjectif']) {
                                    li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin : verso et masculin`);
                                } else {
                                    li.children('.cardDescription').text(`Fiche recto/verso : verso `);
                                }
                            }
                        }
                    } else {
                        if (card[cardKey]['adjectif']) {
                            li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin : recto et masculin`);
                        } else {
                            li.children('.cardDescription').text('Fiche recto/verso : recto ');
                        }
                    }
                } else {
                    if (card[cardKey]['adjectif']) {
                        if (cardKey.length > 2) {
                            li.children('.cardDescription').text(`Fiche simple avec féminin/masculin : feminin`);
                        } else {
                            li.children('.cardDescription').text(`Fiche simple avec féminin/masculin : masculin`);
                        }
                    } else {
                        li.children('.cardDescription').text('Fiche simple :  ');
                    }
                }
            }

            var buttonImp = $(`.cardImageLi-${cardKey}`).children('.flexContainer').children().eq(0);
            buttonImp.addClass(`cardImageButtonImp-${cardKey}`);
            buttonImp.on('click', () => { 
                app.importImage(cardKey) 
                ;
            });

            var buttonRog = $(`.cardImageLi-${cardKey}`).children('.flexContainer').children().eq(1);
            buttonRog.addClass(`cardImageButtonRog-${cardKey}`);
            buttonRog.on('click', () => { 
                app.croppeImage(cardKey)
                ;
            });

            var buttonVal = $(`.cardImageLi-${cardKey}`).children('.flexContainer').children().eq(2);
            buttonVal.addClass(` cardImageButtonVal-${cardKey}`);
            buttonVal.on('click', () => { 
                app.validateImage(cardKey) 
                app.imageFill--;
            });

            var canvas = $(`.cardImageLi-${cardKey}`).children('.flexContainer').children().eq(3);
            canvas.addClass(`cardImageCanvas-${cardKey}`);

            var canvasHQ = $(`.cardImageLi-${cardKey}`).children('.cropperContainer').children().eq(0);
            canvasHQ.addClass(`cardImageCanvasHQ-${cardKey}`);

            var canvasFinal = $(`.cardImageLi-${cardKey}`).children('.flexContainer').children().eq(4);
            canvasFinal.addClass(`cardImageCanvasFinal-${cardKey}`);

            if (undefined != card[cardKey]['imageOriginal']) {
                app.drawApercu(cardKey, card[cardKey]['imageOriginal']);
                app.imageFill--;
            }
        })
    },

    importImage: function (id) {
        console.log('importImage');
        remote.dialog.showOpenDialog(remote.getCurrentWindow(),
            { filters: [{ name: 'Images', extensions: ['png', 'jpg'] }] },
            (filenames) => {
                //read image (note: use async in production)
                data = fs.readFileSync(filenames[0]).toString('base64');

                app.appJSONDescriber.card[id]['imageOriginal'] = data;

                app.drawApercu(id, data);

                return;
            })
    },

    drawApercu: function (id, data) {
        var canvas = $(`.cardImageCanvas-${id}`)[0]
        canvas.width = 50;
        canvas.height = 50;
        var ctx = canvas.getContext('2d');

        var canvasHQ = $(`.cardImageCanvasHQ-${id}`)[0]
        canvasHQ.width = 1920;
        canvasHQ.height = 1080;
        var ctxHQ = canvasHQ.getContext('2d');

        img = new Image();
        img.src = `data:image/png;base64,${data}`;;
        img.onload = function () {
            ctx.drawImage(img, 0, 0, img.width, img.height,   // source rectangle
                0, 0, canvas.width, canvas.height); // destination rectangle
            canvasHQ.width = img.width;
            canvasHQ.height = img.height;
            ctxHQ.drawImage(img, 0, 0, img.width, img.height,   // source rectangle
                0, 0, img.width, img.height); // destination rectangle
        };
    },

    croppeImage: function (id) {
        var canvasHQ = $(`.cardImageCanvasHQ-${id}`)
        canvasHQ.cropper({
            minContainerWidth: screen.width * 1/4,
            minContainerHeight: screen.height * 1/4,
            aspectRatio: 1,
            autoCropArea: 1,
            crop: function (event) {
            }
        });
        var cropper = canvasHQ.data('cropper');
        $(`.cardImageButtonVal-${id}`).show();
    },

    validateImage: function (id) {
        var cropper = $(`.cardImageCanvasHQ-${id}`).data('cropper')
        var data = cropper.getData()
        var canvasFinal = $(`.cardImageCanvasFinal-${id}`)
        var ratio = data.width / data.height;
        canvasFinal.attr('width', '500')
        canvasFinal.attr('height', `${500 / ratio}`)
        var ctxFinal = canvasFinal[0].getContext('2d');
        ctxFinal.clearRect(0, 0, canvasFinal[0].width, canvasFinal[0].height)

        ctxFinal.drawImage(cropper.getCroppedCanvas(), 0, 0, data.width, data.height, 0, 0, canvasFinal[0].width, canvasFinal[0].width / ratio)

        app.appJSONDescriber.card[id]['image64'] = cropper.getCroppedCanvas().toDataURL().split(",")[1];

        $(`.cardImageCanvasHQ-${id}`).cropper('destroy');
    },

    /////////////////// Page Texte Selection 
    /////////////////////////////////////////////////////////////////////////////////////////////////////// 
    loadTextChoicePageHTML: function () {
        console.log("loadImageChoicePageHTML");
        app.textFill=0;
        app.cleanList('cardTextList');

        Object.keys(app.appJSONDescriber.card).sort((a, b) => (a > b) ? 1 : -1).forEach((cardKey) => {
            app.addListenerToTextChoicePageHTML(cardKey)
        })

        var buttonLoadImageChoice = $('.loadImageChoice');
        buttonLoadImageChoice.on('click', () => {
            app.cleanList('appContainer');
            app.appContainer.load('./html/ImageChoicePage/imageChoice.html', () => { app.loadImageChoicePageHTML(true) });
        })

        var buttonLoadLayoutChoice = $('.loadLayoutChoice');
        buttonLoadLayoutChoice.on('click', () => {
            console.log(app.textFill)
            if(app.textFill <= 0){
                app.cleanList('appContainer');
                app.appContainer.load('./html/LayoutChoicePage/layoutChoice.html', () => { app.loadLayoutChoicePageHTML(true) });
            }else{
                Swal.fire({
                    title: 'Error!',
                    text: 'Merci de remplir tous les champs',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            }
        })
    },


    addListenerToTextChoicePageHTML: function (cardKey) {
        console.log("addListenerToTextChoicePageHTML");
        const { card } = app.appJSONDescriber;
        var ul = $('.cardTextList');

        var li = $('<li>');
        li.addClass(`card cardTextLi cardTextLi-${cardKey}`)
        li.appendTo(ul);

        li.load('./html/TextChoicePage/textCardLi.html', () => {
            app.textFill++;
            app.textFill++;
            li.children('.motRappel').text(card[cardKey]['motRappel'])

            if (undefined != card[cardKey]['rectoVerso']) {
                if (card[cardKey]['rectoVerso']) {
                    if (cardKey.length > 2) {
                        array = cardKey.split("-")
                        if (array.length == 3) {
                            li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin:  verso et feminin  `);
                        } else if (array.length == 2) {
                            if (array[1] == "feminin") {
                                li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin: recto et feminin`);
                            } else if (array[1] == "verso") {
                                if (card[cardKey]['adjectif']) {
                                    li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin : verso et masculin`);
                                } else {
                                    li.children('.cardDescription').text(`Fiche recto/verso : verso `);
                                }
                            }
                        }
                    } else {
                        if (card[cardKey]['adjectif']) {
                            li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin : recto et masculin`);
                        } else {
                            li.children('.cardDescription').text('Fiche recto/verso : recto ');
                        }
                    }
                } else {
                    if (card[cardKey]['adjectif']) {
                        if (cardKey.length > 2) {
                            li.children('.cardDescription').text(`Fiche simple avec féminin/masculin : feminin`);
                        } else {
                            li.children('.cardDescription').text(`Fiche simple avec féminin/masculin : masculin`);
                        }
                    } else {
                        li.children('.cardDescription').text('Fiche simple :  ');
                    }
                }
            }

            var textContainer = $(`.cardTextLi-${cardKey}`).children('.textContainer');
            var typeWordContainer = $(`.cardTextLi-${cardKey}`).children('.typeWordChoice');

            var input = textContainer.children('.text-input');
            var select = typeWordContainer.children('.typeWord');

            if (undefined != card[cardKey]['textToDisplay']) {
                input.val(card[cardKey]['textToDisplay']);
                if( input.val()!=""){app.textFill--};

            }
            input.addClass(`text-input-${cardKey}`);
            input.on('focusin', function(){ $(this).data('val', $(this).val());})
            input.on('change', function(e){
                card[cardKey]['textToDisplay'] = e.target.value
                if($(this).data('val')==""){
                    console.log("-- textfill")
                    app.textFill--};
            });

            if (undefined != card[cardKey]['wordType']) {
                select.val(card[cardKey]['wordType']);
                if( select.val()!=""){
                    console.log(select.val());
                    app.textFill--};
            }
            select.addClass(`typeWord-${cardKey}`);
            select.mousedown( function(){console.log( $(this));
                $(this).data('val', $(this).val());})
            select.on('change', function(e){
                console.log("Saving value " + $(this).data('val'));
                card[cardKey]['wordType'] = e.target.value
                if( $(this).data('val')==""){app.textFill--};

            });
        })

    },

    /////////////////// Page Layout Selection 
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    loadLayoutChoicePageHTML: function () {
        console.log("loadLayoutChoicePageHTML");

        app.cleanList('cardLayoutList');
        $('.savePDF').hide()
        $('.loading').hide()
        var tempKey = "";
        Object.keys(app.appJSONDescriber.card).sort((a, b) => (a > b) ? 1 : -1).filter((key) => {
            // initialisation
            if (tempKey.length == 0) {
                tempKey = key
                return key
            } else {
                var firstCharactereKey = key.substr(0, 1)
                var firstCharactereLastKey = tempKey.substr(0, 1)
                if (firstCharactereKey != firstCharactereLastKey) {
                    tempKey = key
                    return key
                }
            }

        }).forEach((cardKey) => {
            console.log("cardKey:", cardKey)
            app.addListenerToLayoutChoicePageHTML(cardKey)
        })

        var buttonLoadTextChoice = $('.loadTextChoice');
        buttonLoadTextChoice.on('click', () => {
            app.cleanList('appContainer');
            app.textFill=0;
            app.appContainer.load('./html/TextChoicePage/textChoice.html', () => { app.loadTextChoicePageHTML(true) });
        })

        var buttonConfirmation = $('.loadConfirmation');
        buttonConfirmation.on('click', () => {
            // app.cleanList('appContainer');
            console.log("JSON:", app.appJSONDescriber)
            app.loadPDFObjectJSON();
        })

        var buttonSave = $('.savePDF');
        buttonSave.on('click', () => {

            app.savePDF();
        })

        var buttonRestart = $('.restart');
        buttonRestart.on('click', () => {
            const remote = require('electron').remote;
            remote.app.relaunch();
            remote.app.exit(0);

        })

    },

    addListenerToLayoutChoicePageHTML: function (cardKey) {
        console.log("addListenerToLayoutChoicePageHTML");
        const { card } = app.appJSONDescriber;
        var ul = $('.cardLayoutList');

        var li = $('<li>');
        li.addClass(`card cardLayoutLi cardLayoutLi-${cardKey}`)
        li.appendTo(ul);

        li.load('./html/LayoutChoicePage/layoutCardLi.html', () => {

            li.children('.motRappel').text(card[cardKey]['motRappel'])

            if (undefined != card[cardKey]['rectoVerso']) {
                if (card[cardKey]['rectoVerso']) {
                    if (cardKey.length > 2) {
                        array = cardKey.split("-")
                        if (array.length == 3) {
                            li.children('.cardDescription').text(`Fiche recto/verso`);
                        } else if (array.length == 2) {
                            if (array[1] == "feminin") {
                                li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin`);
                            } else if (array[1] == "verso") {
                                if (card[cardKey]['adjectif']) {
                                    li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin`);
                                } else {
                                    li.children('.cardDescription').text(`Fiche recto/verso`);
                                }
                            }
                        }
                    } else {
                        if (card[cardKey]['adjectif']) {
                            li.children('.cardDescription').text(`Fiche recto/verso avec féminin/masculin`);
                        } else {
                            li.children('.cardDescription').text('Fiche recto/verso');
                        }
                    }
                } else {
                    if (card[cardKey]['adjectif']) {
                        if (cardKey.length > 2) {
                            li.children('.cardDescription').text(`Fiche simple avec féminin/masculin`);
                        } else {
                            li.children('.cardDescription').text(`Fiche simple avec féminin/masculin`);
                        }
                    } else {
                        li.children('.cardDescription').text('Fiche simple :  ');
                    }
                }
            }

            var layoutChoiceContainer = $(`.cardLayoutLi-${cardKey}`).children('.layoutChoiceContainer');
            var inputA4 = layoutChoiceContainer.children('.A4Container').children('input');
            var inputA5 = layoutChoiceContainer.children('.A5Container').children('input');
            var inputA8 = layoutChoiceContainer.children('.A8Container').children('input');

            if (undefined != card[cardKey]['A4Qty']) {
                inputA4.val(card[cardKey]['A4Qty']);
            }
            if (undefined != card[cardKey]['A5Qty']) {
                inputA5.val(card[cardKey]['A5Qty']);
            }
            if (undefined != card[cardKey]['A8Qty']) {
                inputA8.val(card[cardKey]['A8Qty']);
            }

            inputA4.addClass(`A4-input-${cardKey}`);
            inputA5.addClass(`A5-input-${cardKey}`);
            inputA8.addClass(`A8-input-${cardKey}`);

            inputA4.on("change", (e) => { card[cardKey]['A4Qty'] = e.target.value })
            inputA5.on("change", (e) => { card[cardKey]['A5Qty'] = e.target.value })
            inputA8.on("change", (e) => { card[cardKey]['A8Qty'] = e.target.value })

        })

    },

    /////////////////// Load PDF 
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    loadPDFObjectJSON: function () {


        // load pdf object -----------------------
        var tempKey = "";
        app.printRV = { 'A4': {}, 'A5': {}, 'A8': {} };
        app.printSimple = { 'A4': {}, 'A5': {}, 'A8': {} };

        var rvCardKey = Object.keys(app.appJSONDescriber.card).filter((key) => app.appJSONDescriber.card[key].rectoVerso == true).sort((a, b) => (a > b) ? 1 : -1)

        if (undefined != rvCardKey) {
            var rvCardKeyPrimare = rvCardKey.filter((key) => {
                // initialisation
                if (tempKey.length == 0) {
                    tempKey = key
                    return key
                } else {
                    var firstCharactereKey = key.substr(0, 1)
                    var firstCharactereLastKey = tempKey.substr(0, 1)
                    if (firstCharactereKey != firstCharactereLastKey) {
                        tempKey = key
                        return key
                    }
                }
            })
            var countPageRV = 1;
            var i = 0
            var arrayAlreadyPush = []
            rvCardKeyPrimare.forEach(() => {
                rvCardKey.forEach((key) => {
                    i = 0
                    var firstLetter = key.substr(0, 1);
                    // find the number of the card number we will check
                    while (i != firstLetter || arrayAlreadyPush.includes(i)) {
                        i++
                        if (i == 300) {
                            break;
                        }
                    }
                    if (i != 300) {
                        arrayAlreadyPush.push(i)
                        for (let y = 0; y < app.appJSONDescriber.card[i]['A4Qty']; y++) {

                            app.printRV['A4'][countPageRV] = {}
                            app.printRV['A4'][countPageRV][i] = app.appJSONDescriber.card[i]
                            if (app.appJSONDescriber.card[i].adjectif) {
                                app.printRV['A4'][countPageRV][`${i}-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                            }
                            countPageRV++;
                            app.printRV['A4'][countPageRV] = {}
                            app.printRV['A4'][countPageRV][`${i}-verso`] = app.appJSONDescriber.card[`${i}-verso`]
                            if (app.appJSONDescriber.card[i].adjectif) {
                                app.printRV['A4'][countPageRV][`${i}-verso-feminin`] = app.appJSONDescriber.card[`${i}-verso-feminin`]
                            }
                            countPageRV++;

                        }
                        for (let y = 0; y < app.appJSONDescriber.card[i]['A5Qty']; y++) {
                            // check if this page number contain something if not create an empty
                            if (undefined == app.printRV['A5'][countPageRV]) {
                                // add a new page to do the recto 
                                // console.log("app.printRV['A5'][",countPageRV,"] = {};")
                                app.printRV['A5'][countPageRV] = {};
                            }

                            // let's do the recto
                            if (undefined != app.printRV['A5'][countPageRV][i]) {

                                // console.log("app.printRV['A5'][",countPageRV,"][,",i,"-2]=: ",app.appJSONDescriber.card[i])
                                app.printRV['A5'][countPageRV][`${i}-2`] = app.appJSONDescriber.card[i]

                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printRV['A5'][countPageRV][`${i}-2-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                }
                            } else {
                                // console.log("app.printRV['A5'][",countPageRV,"][,",i,"]=: ",app.appJSONDescriber.card[i])

                                app.printRV['A5'][countPageRV][i] = app.appJSONDescriber.card[i]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printRV['A5'][countPageRV][`${i}-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                }
                            }

                            // check if this page number+1 contain something if not create an empty
                            if (undefined == app.printRV['A5'][countPageRV + 1]) {
                                // add a new page to do the verso 
                                // console.log("app.printRV['A5'][",countPageRV,"+1] = {};")

                                app.printRV['A5'][countPageRV + 1] = {}
                            }
                            if (undefined == app.printRV['A5'][countPageRV + 1][`${i}-verso`]) {
                                // let's do the verso
                                // console.log("app.printRV['A5'][",countPageRV,"+1][,",i,"-verso]=: ",app.appJSONDescriber.card[`${i}-verso`])

                                app.printRV['A5'][countPageRV + 1][`${i}-verso`] = app.appJSONDescriber.card[`${i}-verso`]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printRV['A5'][countPageRV + 1][`${i}-verso-feminin`] = app.appJSONDescriber.card[`${i}-verso-feminin`]
                                }
                            } else {
                                // console.log("app.printRV['A5'][",countPageRV,"+1][,",i,"-2-verso]=: ",app.appJSONDescriber.card[`${i}-verso`])


                                app.printRV['A5'][countPageRV + 1][`${i}-2-verso`] = app.appJSONDescriber.card[`${i}-verso`]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printRV['A5'][countPageRV + 1][`${i}-2-verso-feminin`] = app.appJSONDescriber.card[`${i}-verso-feminin`]
                                }
                                countPageRV = countPageRV + 2;
                            }
                        }
                        for (let y = 0; y < app.appJSONDescriber.card[i]['A8Qty']; y++) {
                            // check if this page number contain something if not create an empty
                            if (undefined == app.printRV['A8'][countPageRV]) {
                                // add a new page to do the recto 
                                app.printRV['A8'][countPageRV] = {};
                            }

                            // let's do the recto
                            if (undefined != app.printRV['A8'][countPageRV][i]) {
                                if (undefined != app.printRV['A8'][countPageRV][`${i}-2`]) {
                                    if (undefined != app.printRV['A8'][countPageRV][`${i}-3`]) {
                                        app.printRV['A8'][countPageRV][`${i}-4`] = app.appJSONDescriber.card[i]
                                        if (app.appJSONDescriber.card[i].adjectif) {
                                            app.printRV['A8'][countPageRV][`${i}-4-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                        }
                                    } else {
                                        app.printRV['A8'][countPageRV][`${i}-3`] = app.appJSONDescriber.card[i]
                                        if (app.appJSONDescriber.card[i].adjectif) {
                                            app.printRV['A8'][countPageRV][`${i}-3-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                        }
                                    }
                                } else {
                                    app.printRV['A8'][countPageRV][`${i}-2`] = app.appJSONDescriber.card[i]
                                    if (app.appJSONDescriber.card[i].adjectif) {
                                        app.printRV['A8'][countPageRV][`${i}-2-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                    }
                                }
                            } else {
                                app.printRV['A8'][countPageRV][i] = app.appJSONDescriber.card[i]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printRV['A8'][countPageRV][`${i}-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                }
                            }

                            // check if this page number+1 contain something if not create an empty
                            if (undefined == app.printRV['A8'][countPageRV + 1]) {
                                // add a new page to do the verso 
                                app.printRV['A8'][countPageRV + 1] = {}
                            }

                            if (undefined != app.printRV['A8'][countPageRV + 1][`${i}-verso`]) {
                                if (undefined != app.printRV['A8'][countPageRV + 1][`${i}-2-verso`]) {
                                    if (undefined != app.printRV['A8'][countPageRV + 1][`${i}-3-verso`]) {
                                        app.printRV['A8'][countPageRV + 1][`${i}-4-verso`] = app.appJSONDescriber.card[`${i}-verso`]
                                        if (app.appJSONDescriber.card[i].adjectif) {
                                            app.printRV['A8'][countPageRV + 1][`${i}-4-verso-feminin`] = app.appJSONDescriber.card[`${i}-verso-feminin`]
                                        }
                                        countPageRV = countPageRV + 2
                                    } else {
                                        app.printRV['A8'][countPageRV + 1][`${i}-3-verso`] = app.appJSONDescriber.card[`${i}-verso`]
                                        if (app.appJSONDescriber.card[i].adjectif) {
                                            app.printRV['A8'][countPageRV + 1][`${i}-3-verso-feminin`] = app.appJSONDescriber.card[`${i}-verso-feminin`]
                                        }
                                    }
                                } else {
                                    app.printRV['A8'][countPageRV + 1][`${i}-2-verso`] = app.appJSONDescriber.card[`${i}-verso`]
                                    if (app.appJSONDescriber.card[i].adjectif) {
                                        app.printRV['A8'][countPageRV + 1][`${i}-2-verso-feminin`] = app.appJSONDescriber.card[`${i}-verso-feminin`]
                                    }
                                }
                            } else {
                                app.printRV['A8'][countPageRV + 1][`${i}-verso`] = app.appJSONDescriber.card[`${i}-verso`]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printRV['A8'][countPageRV + 1][`${i}-verso-feminin`] = app.appJSONDescriber.card[`${i}-verso-feminin`]
                                }
                            }
                        }
                    } else {
                        return;
                    }
                })
            })

        }

        var simpleCardKey = Object.keys(app.appJSONDescriber.card).filter((key) => app.appJSONDescriber.card[key].rectoVerso == false).sort((a, b) => (a > b) ? 1 : -1)
        if (undefined != simpleCardKey) {
            var simpleCardKeyPrimare = simpleCardKey.filter((key) => {
                // initialisation
                if (tempKey.length == 0) {
                    tempKey = key
                    return key
                } else {
                    var firstCharactereKey = key.substr(0, 1)
                    var firstCharactereLastKey = tempKey.substr(0, 1)
                    if (firstCharactereKey != firstCharactereLastKey) {
                        tempKey = key
                        return key
                    }
                }
            })
            var countPage = 1;
            var i = 0
            var arrayAlreadyPush = []
            simpleCardKeyPrimare.forEach(() => {
                simpleCardKey.forEach((key) => {
                    i = 0
                    var firstLetter = key.substr(0, 1);
                    // find the number of the card number we will check
                    while (i != firstLetter || arrayAlreadyPush.includes(i)) {
                        i++
                        if (i == 300) {
                            break;
                        }
                    }
                    if (i != 300) {
                        arrayAlreadyPush.push(i)
                        for (let y = 0; y < app.appJSONDescriber.card[i]['A4Qty']; y++) {

                            app.printSimple['A4'][countPage] = {}
                            app.printSimple['A4'][countPage][i] = app.appJSONDescriber.card[i]
                            if (app.appJSONDescriber.card[i].adjectif) {
                                app.printSimple['A4'][countPage][`${i}-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                            }
                            countPage++;

                        }
                        for (let y = 0; y < app.appJSONDescriber.card[i]['A5Qty']; y++) {
                            // check if this page number contain something if not create an empty
                            if (undefined == app.printSimple['A5'][countPage]) {
                                // add a new page to do the recto 
                                app.printSimple['A5'][countPage] = {};
                            }

                            // let's do the recto
                            if (undefined != app.printSimple['A5'][countPage][i]) {
                                app.printSimple['A5'][countPage][`${i}-2`] = app.appJSONDescriber.card[i]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printSimple['A5'][countPage][`${i}-2-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                }
                                countPage++;
                            } else {
                                app.printSimple['A5'][countPage][i] = app.appJSONDescriber.card[i]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printSimple['A5'][countPage][`${i}-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                }

                            }
                        }
                        for (let y = 0; y < app.appJSONDescriber.card[i]['A8Qty']; y++) {
                            // check if this page number contain something if not create an empty
                            if (undefined == app.printSimple['A8'][countPage]) {
                                // add a new page to do the recto 
                                app.printSimple['A8'][countPage] = {};
                            }
                            // let's do the recto
                            if (undefined != app.printSimple['A8'][countPage][i]) {
                                if (undefined != app.printSimple['A8'][countPage][`${i}-2`]) {
                                    if (undefined != app.printSimple['A8'][countPage][`${i}-3`]) {
                                        app.printSimple['A8'][countPage][`${i}-4`] = app.appJSONDescriber.card[i]
                                        if (app.appJSONDescriber.card[i].adjectif) {
                                            app.printSimple['A8'][countPage][`${i}-4-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                        }
                                        countPage++;
                                    } else {
                                        app.printSimple['A8'][countPage][`${i}-3`] = app.appJSONDescriber.card[i]
                                        if (app.appJSONDescriber.card[i].adjectif) {
                                            app.printSimple['A8'][countPage][`${i}-3-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                        }
                                    }
                                } else {
                                    app.printSimple['A8'][countPage][`${i}-2`] = app.appJSONDescriber.card[i]
                                    if (app.appJSONDescriber.card[i].adjectif) {
                                        app.printSimple['A8'][countPage][`${i}-2-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                    }
                                }
                            } else {
                                app.printSimple['A8'][countPage][i] = app.appJSONDescriber.card[i]
                                if (app.appJSONDescriber.card[i].adjectif) {
                                    app.printSimple['A8'][countPage][`${i}-feminin`] = app.appJSONDescriber.card[`${i}-feminin`]
                                }
                            }
                        }
                    } else {
                        return;
                    }
                })
            })
        }
        // end load pdf object -----------------------
        app.loadPDF();
    },

    createCanvasToPrint: function (canvasId, pageObj, pdf, pageNumber, type) {
        console.log("createCanvasToPrint:", pageNumber, "type:", type)
        app.launchCanvasCreation++;
        $('.loading').show();
        var imageB64 = `data:image/png;base64,${pageObj['image64']}`
        var text = pageObj['textToDisplay'];
        var wordType = pageObj['wordType'];

        var canvasHidden = document.getElementById(canvasId)
        var ctx = canvasHidden.getContext('2d');
        // ADJUST QUALITY
        ////////////////////////
        var qualityRatioA4 = 2;
        // A4 format
        var globalWidth = qualityRatioA4 * 1000;
        var globalHeight = qualityRatioA4 * 1000;
        canvasHidden.width = globalWidth;
        canvasHidden.height = globalHeight;
        ctx.scale(qualityRatioA4, qualityRatioA4);


        // ADD IMAGE
        ////////////////////////
        var img = new Image();
        img.src = imageB64;
        var leftImageLimit = 130;
        var spaceAllowForImage = 730;
        var topStartImage = 120;
        var heightAllowToImage = 280;
        img.onload = function () {
            var imageRatio = img.height / img.width
            console.log("imageRatio:",imageRatio);
            if (img.width > spaceAllowForImage) {
                // more height after resize more width   
                if (spaceAllowForImage * imageRatio > heightAllowToImage) {
                    ctx.drawImage(img, leftImageLimit + (spaceAllowForImage - heightAllowToImage / imageRatio) / 2, topStartImage, heightAllowToImage / imageRatio, heightAllowToImage)
                    // less height after resize more width   
                    console.log("1");
                } else {
                    ctx.drawImage(imgTop, leftImageLimit, topStartImage + (heightAllowToImage - spaceAllowForImage * imageRatio) / 2, spaceAllowForImage, spaceAllowForImage * imageRatio)
                    console.log("2");
                }
            } else {
                // more height less width
                if (img.height > heightAllowToImage) {
                    ctx.drawImage(img, leftImageLimit + (spaceAllowForImage - heightAllowToImage / imageRatio) / 2, topStartImage, heightAllowToImage / imageRatio, heightAllowToImage)
                    // less height less width
                } else {
                    ctx.drawImage(img, leftImageLimit + (spaceAllowForImage - img.width) / 2, topStartImage + (heightAllowToImage - img.height) / 2, img.width, img.height)
                    console.log("4");
                }
            }
            app.addTextForSimpleCard(ctx, text, globalWidth, globalHeight, qualityRatioA4, wordType)
            app.addThisCanvasToPDF(canvasHidden.toDataURL(), pdf, pageNumber, type)
        };

    },

    addTextForSimpleCard: function (ctx, text, globalWidth, globalHeight, qualityRatioA4, wordType) {
        // ADJUST TEXT
        ////////////////////////
        // get text
        var arraySplit = text.split('/');

        var fontSize = 100;
        var space = 0.4 * fontSize;
        var spaceAllowForText = 730;
        var initialSpace = 0;
        var leftTextLimit = 130;

        // ajust font size according to the text
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`
        var widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))

        if (space * (widthArrayCAP.length - 1) + widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue) > spaceAllowForText) {
            while (space * (widthArrayCAP.length - 1) + widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue) > spaceAllowForText) {
                fontSize = fontSize - 0.1;
                space = 0.4 * fontSize
                ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`;
                widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
            }

        } else {
            initialSpace = (spaceAllowForText - space * (widthArrayCAP.length - 1) - widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue)) / 2
        }

        // get all width and heigth data
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`;
        widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxCAP = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxCAP < h) {
                heigthMaxCAP = h
            }
        })
        // get all width and heigth data
        ctx.font = `${fontSize}pt Conv_CLEO-GS-SCRIPT`
        var widthArraySCRIPT = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxSCRIPT = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxSCRIPT < h) {
                heigthMaxSCRIPT = h
            }
        })
        // get all width and heigth data
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CURSIVE`
        var widthArrayCURSIVE = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxCURSIVE = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxCURSIVE < h) {
                heigthMaxCURSIVE = h
            }
        })
        // end adjust text ------------

        // bandeau
        ctx.fillStyle = app.color[wordType];
        ctx.fillRect(0, 0, 1000, 80);

        // square all the A4 PAGE
        ctx.lineWidth = "10";
        ctx.strokeStyle = "black";
        ctx.rect(0, 0, globalWidth / qualityRatioA4, globalHeight / qualityRatioA4);
        ctx.stroke();

        // ctx.font = '10pt Conv_CLEO-GS-CURSIVE';
        // app.drawGrid(ctx, globalWidth, globalHeight, 20)

        ///////////////////////////////////////////////////
        // CARD TEXT FILL 
        ///////////////////////////////////////////////////

        // CAP FILL 
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / 100
        var middle = 480;
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 0.85;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 0.85;
        var offsetBack = 5 * ratio;
        var paddingLeftBackBeige = 5 * ratio;
        var radius = 20 * ratio;

        arraySplit.forEach((partWord) => {

            // rect background fill
            app.drawRondRect((leftTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArrayCAP[i] * coeffWidthBack, heigthMaxCAP * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArrayCAP[i] * coeffWidthBack, heigthMaxCAP * coeffHeightBack, radius, ctx, "beige")
            ctx.fillStyle = "black";

            // text fill
            ctx.fillText(partWord, leftTextLimit + counter, middle);
            counter = counter + widthArrayCAP[i] + space
            i++

        })

        // SCRIPT FILL 
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-SCRIPT`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / 100
        var middle = 610 - 100 * (1 - ratio);
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 1.15;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 0.95;
        var offsetBack = 5 * ratio;// ok
        var paddingLeftBackBeige = 5 * ratio;
        var radius = 20 * ratio;

        arraySplit.forEach((partWord) => {

            // rect background fill
            app.drawRondRect((leftTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArraySCRIPT[i] * coeffWidthBack, heigthMaxSCRIPT * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArraySCRIPT[i] * coeffWidthBack, heigthMaxSCRIPT * coeffHeightBack, radius, ctx, "beige")

            // text fill
            ctx.fillStyle = "black";
            ctx.fillText(partWord, leftTextLimit + counter, middle);
            counter = counter + widthArraySCRIPT[i] + space
            i++
        })

        // CURSIVE FILL 
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CURSIVE`;
        var counter = initialSpace;
        var i = 0;
        var ratio = fontSize / 100;
        var middle = 885 - 300 * (1 - ratio);
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 2;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 2.6;
        var offsetBack = 5 * ratio;
        var paddingLeftBackBeige = 5 * ratio;
        var radius = 20 * ratio;

        arraySplit.forEach((partWord) => {

            // rect background fill
            app.drawRondRect((leftTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArrayCURSIVE[i] * coeffWidthBack, heigthMaxCURSIVE * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArrayCURSIVE[i] * coeffWidthBack, heigthMaxCURSIVE * coeffHeightBack, radius, ctx, "beige")

            // text fill
            ctx.fillStyle = "black";
            ctx.fillText(partWord, leftTextLimit + counter, middle);
            counter = counter + widthArrayCURSIVE[i] + space
            i++
        })
    },
    createCanvasToPrintDoubleCard: function (canvasId, pageObjTop, pageObjBot, pdf, pageNumber, type) {
        console.log("createCanvasToPrintDoubleCard:", pageNumber, "type:", type)
        app.launchCanvasCreation++;
        var imageB64Top = `data:image/png;base64,${pageObjTop['image64']}`
        var imageB64Bot = `data:image/png;base64,${pageObjBot['image64']}`

        var textTop = pageObjTop['textToDisplay'];
        var textBot = pageObjBot['textToDisplay'];
        var wordType = pageObjTop['wordType'];
        var canvasHidden = document.getElementById(canvasId);
        var ctx = canvasHidden.getContext('2d');

        // ADJUST QUALITY
        ////////////////////////
        var qualityRatioA4 = 2;
        // A4 format
        var globalWidth = qualityRatioA4 * 1000;
        var globalHeight = qualityRatioA4 * 1000;
        canvasHidden.width = globalWidth;
        canvasHidden.height = globalHeight;
        ctx.scale(qualityRatioA4, qualityRatioA4);


        // ADD IMAGE
        ////////////////////////

        // Image Top --------------------------------
        var imgTop = new Image();
        app.imgTopLoad = false;
        imgTop.src = imageB64Top;
        var leftImageLimitTop = 40;
        var spaceAllowForImageTop = 240;
        var topStartImage = 120;
        var heightAllowToImageBot = 360;
        imgTop.onload = function () {
            var imageRatioTop = imgTop.height / imgTop.width
            if (imgTop.width > spaceAllowForImageTop) {
                // more height after resize more width   
                if (spaceAllowForImageTop * imageRatioTop > heightAllowToImageBot) {
                    ctx.drawImage(imgTop, leftImageLimitTop + (spaceAllowForImageTop - heightAllowToImageBot / imageRatioTop) / 2, topStartImage, heightAllowToImageBot / imageRatioTop, heightAllowToImageBot)

                    // less height after resize more width   
                } else {
                    ctx.drawImage(imgTop, leftImageLimitTop, topStartImage + (heightAllowToImageBot - spaceAllowForImageTop * imageRatioTop) / 2, spaceAllowForImageTop, spaceAllowForImageTop * imageRatioTop)

                }
            } else {
                // more height less width
                if (imgTop.height > heightAllowToImageBot) {
                    ctx.drawImage(imgTop, leftImageLimitTop + (spaceAllowForImageTop - heightAllowToImageBot / imageRatioTop) / 2, topStartImage, heightAllowToImageBot / imageRatioTop, heightAllowToImageBot)
                    // less height less width

                } else {
                    ctx.drawImage(imgTop, leftImageLimitTop + (spaceAllowForImageTop - imgTop.width) / 2, topStartImage + (heightAllowToImageBot - imgTop.height) / 2, imgTop.width, imgTop.height)

                }
            }
            app.imgTopLoad = true
            if (app.imgBotLoad) {
                app.addTextForDoubleCard(ctx, textTop, textBot, globalWidth, globalHeight, qualityRatioA4, wordType)
                app.addThisCanvasToPDF(canvasHidden.toDataURL(), pdf, pageNumber, type)
                app.imgTopLoad = false;
            }
        };

        // Image Bot --------------------------------
        var imgBot = new Image();
        imgBot.src = imageB64Bot;
        var leftImageLimitBot = 720;
        var spaceAllowForImageBot = 240;
        var botStartImage = 600;
        app.imgBotLoad = false;
        var heightAllowToImageBot = 360;
        imgBot.onload = function () {

            var imageRatioBot = imgBot.height / imgBot.width
            if (imgBot.width > spaceAllowForImageBot) {
                // more height after resize more width   
                if (spaceAllowForImageBot * imageRatioBot > heightAllowToImageBot) {
                    ctx.drawImage(imgBot, leftImageLimitBot + (spaceAllowForImageBot - heightAllowToImageBot / imageRatioBot) / 2, botStartImage, heightAllowToImageBot / imageRatioBot, heightAllowToImageBot)
                    // less height after resize more width   
                } else {
                    ctx.drawImage(imgBot, leftImageLimitBot, botStartImage + (heightAllowToImageBot - spaceAllowForImageBot * imageRatioBot) / 2, spaceAllowForImageBot, spaceAllowForImageBot * imageRatioBot)
                }
            } else {
                // more height less width
                if (imgBot.height > heightAllowToImageBot) {
                    ctx.drawImage(imgBot, leftImageLimitBot + (spaceAllowForImageBot - imgBot.width) / 2, botStartImage, heightAllowToImageBot / imageRatio, heightAllowToImageBot)
                    // less height less width
                } else {
                    ctx.drawImage(imgBot, leftImageLimitBot + (spaceAllowForImageBot - imgBot.width) / 2, botStartImage + (heightAllowToImageBot - imgBot.height) / 2, imgBot.width, imgBot.height)
                }
            }
            app.imgBotLoad = true
            if (app.imgTopLoad) {
                app.addTextForDoubleCard(ctx, textTop, textBot, globalWidth, globalHeight, qualityRatioA4, wordType)
                app.addThisCanvasToPDF(canvasHidden.toDataURL(), pdf, pageNumber, type)
                app.imgBotLoad = false;
            }
        };


    },
    addTextForDoubleCard: function (ctx, textTop, textBot, globalWidth, globalHeight, qualityRatioA4, wordType) {
        // ADJUST TEXT
        ////////////////////////

        // get text
        var arraySplit = textTop.split('/');

        var fontSize = 75;
        var fontSizeMax = 75;
        var space = 0.4 * fontSize;
        var spaceAllowForText = 640;
        var initialSpace = 0;
        var leftTopTextLimit = 320;
        var leftBotTextLimit = 40;

        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`
        var widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))

        if (space * (widthArrayCAP.length - 1) + widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue) > spaceAllowForText) {
            while (space * (widthArrayCAP.length - 1) + widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue) > spaceAllowForText) {
                fontSize = fontSize - 0.1;
                space = 0.4 * fontSize
                ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`;
                widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
            }
            console.log(fontSize)
        } else {
            initialSpace = (spaceAllowForText - space * (widthArrayCAP.length - 1) - widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue)) / 2
        }

        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`;
        widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxCAP = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxCAP < h) {
                heigthMaxCAP = h
            }
        })
        ctx.font = `${fontSize}pt Conv_CLEO-GS-SCRIPT`
        var widthArraySCRIPT = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxSCRIPT = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxSCRIPT < h) {
                heigthMaxSCRIPT = h
            }
        })
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CURSIVE`
        var widthArrayCURSIVE = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxCURSIVE = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxCURSIVE < h) {
                heigthMaxCURSIVE = h
            }
        })
        // end adjust text ------------

        // bandeau
        ctx.fillStyle = app.color[wordType];
        ctx.fillRect(0, 0, 1000, 80);

        // bandeau milieu
        ctx.fillStyle = app.color[wordType];
        ctx.fillRect(0, 520, 1000, 40);

        // square all the A4 PAGE
        ctx.lineWidth = "5";
        ctx.strokeStyle = "black";
        ctx.rect(0, 0, globalWidth / qualityRatioA4, globalHeight / qualityRatioA4);
        ctx.stroke();

        // ctx.font = '10pt Conv_CLEO-GS-CURSIVE';
        // app.drawGrid(ctx, globalWidth, globalHeight, 20)




        ////////////////////////////////////////
        //// TOP TEXT
        ////////////////////////////////////////

        arraySplit = textTop.split('/');

        // CAP FILL TOP
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / fontSizeMax
        var middle = 130;
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 0.85;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 0.85;//ok
        var offsetBack = 5 * ratio;// ok
        var paddingLeftBackBeige = 5 * ratio;//ok
        var radius = 20 * ratio;//ok
        arraySplit.forEach((partWord) => {

            app.drawRondRect((leftTopTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArrayCAP[i] * coeffWidthBack, heigthMaxCAP * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftTopTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArrayCAP[i] * coeffWidthBack, heigthMaxCAP * coeffHeightBack, radius, ctx, "beige")
            ctx.fillStyle = "black";

            ctx.fillText(partWord, leftTopTextLimit + counter, middle);
            counter = counter + widthArrayCAP[i] + space
            i++

        })

        // SCRIPT FILL TOP
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-SCRIPT`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / fontSizeMax
        var middle = 230 - 25 * (1 - ratio);
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 1.15;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 0.95;//ok
        var offsetBack = 5 * ratio;// ok
        var paddingLeftBackBeige = 5 * ratio;//ok
        var radius = 20 * ratio;//ok
        arraySplit.forEach((partWord) => {
            app.drawRondRect((leftTopTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArraySCRIPT[i] * coeffWidthBack, heigthMaxSCRIPT * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftTopTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArraySCRIPT[i] * coeffWidthBack, heigthMaxSCRIPT * coeffHeightBack, radius, ctx, "beige")
            ctx.fillStyle = "black";
            ctx.fillText(partWord, leftTopTextLimit + counter, middle);
            counter = counter + widthArraySCRIPT[i] + space
            i++
        })

        // CURSIVE FILL TOP
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CURSIVE`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / fontSizeMax
        var middle = 440 - 150 * (1 - ratio);
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 2;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 2.6;
        var offsetBack = 5 * ratio;
        var paddingLeftBackBeige = 5 * ratio;
        var radius = 20 * ratio;
        arraySplit.forEach((partWord) => {

            // rect background fill
            app.drawRondRect((leftTopTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArrayCURSIVE[i] * coeffWidthBack, heigthMaxCURSIVE * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftTopTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArrayCURSIVE[i] * coeffWidthBack, heigthMaxCURSIVE * coeffHeightBack, radius, ctx, "beige")

            // text fill
            ctx.fillStyle = "black";
            ctx.fillText(partWord, leftTopTextLimit + counter, middle);
            counter = counter + widthArrayCURSIVE[i] + space
            i++
        })

        ////////////////////////////////////////
        //// BOTTOM TEXT
        ////////////////////////////////////////

        // ADJUST TEXT
        ////////////////////////

        // get text
        var arraySplit = textBot.split('/');

        var fontSize = 75;
        var fontSizeMax = 75;
        var space = 0.4 * fontSize;
        var spaceAllowForText = 640;
        var initialSpace = 0;
        var leftTopTextLimit = 320;
        var leftBotTextLimit = 40;

        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`
        var widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))

        if (space * (widthArrayCAP.length - 1) + widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue) > spaceAllowForText) {
            while (space * (widthArrayCAP.length - 1) + widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue) > spaceAllowForText) {
                fontSize = fontSize - 0.1;
                space = 0.4 * fontSize
                ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`;
                widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
            }
            console.log(fontSize)
        } else {
            initialSpace = (spaceAllowForText - space * (widthArrayCAP.length - 1) - widthArrayCAP.reduce((accumulator, currentValue) => accumulator + currentValue)) / 2
        }

        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`;
        widthArrayCAP = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxCAP = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxCAP < h) {
                heigthMaxCAP = h
            }
        })
        ctx.font = `${fontSize}pt Conv_CLEO-GS-SCRIPT`
        var widthArraySCRIPT = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxSCRIPT = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxSCRIPT < h) {
                heigthMaxSCRIPT = h
            }
        })
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CURSIVE`
        var widthArrayCURSIVE = arraySplit.map((word) => Math.round(ctx.measureText(word).width))
        var heigthMaxCURSIVE = 0;
        arraySplit.map((word) => Math.round(parseInt(ctx.font))).forEach((h) => {
            if (heigthMaxCURSIVE < h) {
                heigthMaxCURSIVE = h
            }
        })
        // end adjust text ------------


        // CAP BOTTOM FILL 
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CAP`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / fontSizeMax
        var middle = 610;
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 0.85;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 0.85;
        var offsetBack = 5 * ratio;
        var paddingLeftBackBeige = 5 * ratio;
        var radius = 20 * ratio;
        arraySplit.forEach((partWord) => {

            // rect background fill
            app.drawRondRect((leftBotTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArrayCAP[i] * coeffWidthBack, heigthMaxCAP * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftBotTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArrayCAP[i] * coeffWidthBack, heigthMaxCAP * coeffHeightBack, radius, ctx, "beige")

            // text fill
            ctx.fillStyle = "black";
            ctx.fillText(partWord, leftBotTextLimit + counter, middle);
            counter = counter + widthArrayCAP[i] + space
            i++
        })

        // SCRIPT BOTTOM FILL 
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-SCRIPT`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / fontSizeMax
        var middle = 710 - 25 * (1 - ratio);
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 1.15;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 0.95;
        var offsetBack = 5 * ratio;
        var paddingLeftBackBeige = 5 * ratio;
        var radius = 20 * ratio;

        arraySplit.forEach((partWord) => {

            // rect background fill
            app.drawRondRect((leftBotTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArraySCRIPT[i] * coeffWidthBack, heigthMaxSCRIPT * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftBotTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArraySCRIPT[i] * coeffWidthBack, heigthMaxSCRIPT * coeffHeightBack, radius, ctx, "beige")

            // text fill
            ctx.fillStyle = "black";
            ctx.fillText(partWord, leftBotTextLimit + counter, middle);
            counter = counter + widthArraySCRIPT[i] + space
            i++
        })

        // CURSIVE FILL BOTTOM
        ////////////////////////
        ctx.font = `${fontSize}pt Conv_CLEO-GS-CURSIVE`
        var counter = initialSpace;
        var i = 0
        var ratio = fontSize / fontSizeMax
        var middle = 920 - 150 * (1 - ratio);
        var coeffWidthBack = 1.05;
        var coeffHeightBack = 2;
        var beginTopBack = middle - (parseInt(ctx.font) / 2) * 2.6;
        var offsetBack = 5 * ratio;
        var paddingLeftBackBeige = 5 * ratio;
        var radius = 20 * ratio;

        arraySplit.forEach((partWord) => {

            // rect background fill
            app.drawRondRect((leftBotTextLimit + counter) - paddingLeftBackBeige + offsetBack, beginTopBack + offsetBack, widthArrayCURSIVE[i] * coeffWidthBack, heigthMaxCURSIVE * coeffHeightBack, radius, ctx, "black")
            app.drawRondRect((leftBotTextLimit + counter) - paddingLeftBackBeige, beginTopBack, widthArrayCURSIVE[i] * coeffWidthBack, heigthMaxCURSIVE * coeffHeightBack, radius, ctx, "beige")

            // text fill
            ctx.fillStyle = "black";
            ctx.fillText(partWord, leftBotTextLimit + counter, middle);
            counter = counter + widthArrayCURSIVE[i] + space
            i++
        })

    },

    loadPDF: function () {
        console.log(app.printRV)
        console.log(app.printSimple)

        app.pdfWithRV = new jsPDF();
        app.pdfWithRV.deletePage(1)
        app.pdfSimple = new jsPDF();
        app.pdfSimple.deletePage(1)

        if (undefined != app.printRV) {
            Object.keys(app.printRV).forEach((key) => {
                if (Object.keys(app.printRV[key]).length != 0) {
                    if (key == "A4") {
                        Object.keys(app.printRV[key]).forEach((page) => {
                            // mean there is an double word on the card
                            if (Object.keys(app.printRV[key][page]).length == 2) {
                                var pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                var pageTop = app.printRV[key][page][`${Object.keys(app.printRV[key][page])[0]}`]
                                var pageBot = app.printRV[key][page][`${Object.keys(app.printRV[key][page])[1]}`];
                                app.pdfWithRV.addPage();
                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A4")
                            } else if (Object.keys(app.printRV[key][page]).length == 1) {
                                var pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                var pageSimple = app.printRV[key][page][`${Object.keys(app.printRV[key][page])[0]}`]
                                app.pdfWithRV.addPage();
                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A4")
                            } else {
                                console.log("error")
                            }
                        })
                    } else if (key == "A5") {
                        // mean there is an double word on the card
                        Object.keys(app.printRV[key]).forEach((page) => {
                            // page are 1,2,3,4

                            app.pdfWithRV.addPage();

                            console.log("page:", page)

                            Object.keys(app.printRV[key][page]).forEach(keyPage => {
                                // key page are 1- 1-feminin ...
                                // key with feminin
                                if (keyPage.substr(-8) == "-feminin") {
                                    var keyNormal = keyPage.replace("-feminin", "");
                                    var keyFeminin = keyPage;
                                    var pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                    var pageTop = app.printRV[key][page][keyNormal]
                                    var pageBot = app.printRV[key][page][keyFeminin];
                                    if (Object.keys(app.printRV[key][page]).filter((k) => k.substr(-8) == "-feminin").length == 1) {
                                        pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A5Top")

                                    } else if (Object.keys(app.printRV[key][page]).filter((k) => k.substr(-8) == "-feminin").length == 2) {
                                        // like 1- and 1-2     or      1- and 2- 
                                        if (Object.keys(app.printRV[key][page])[0].substr(0, 1) == Object.keys(app.printRV[key][page])[1].substr(0, 1)) {
                                            if (keyPage.split("-")[1] == "2") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A5Bot")
                                            } else {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A5Top")
                                            }
                                            // like  1- and 2- 
                                        } else {
                                            if (keyPage.substr(0, 1) == "1") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A5Top")
                                            } else {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A5Bot")
                                            }
                                        }
                                    }
                                    // key without feminin
                                } else if (!Object.keys(app.printRV[key][page]).includes(`${keyPage}-feminin`)) {
                                    var pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                    var pageSimple = app.printRV[key][page][keyPage]

                                    if (Object.keys(app.printRV[key][page]).filter((k) => k.substr(-8) != "-feminin").length == 1) {
                                        pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                        app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A5Top")

                                    } else if (Object.keys(app.printRV[key][page]).filter((k) => k.substr(-8) != "-feminin").length == 2) {
                                        // like 1- and 1-2
                                        if (Object.keys(app.printRV[key][page])[0].substr(0, 1) == Object.keys(app.printRV[key][page])[1].substr(0, 1)) {
                                            if (keyPage.split("-")[1] == "2") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A5Bot")
                                            } else {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A5Top")
                                            }

                                            // like  1- and 2- 
                                        } else {
                                            if (keyPage.substr(0, 1) == "1") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A5Top")
                                            } else {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A5Bot")
                                            }
                                        }
                                    }
                                }
                            });
                        })
                    } else if (key == "A8") {
                        Object.keys(app.printRV[key]).forEach((page) => {
                            // page are 1,2,3,4
                            app.pdfWithRV.addPage();
                            Object.keys(app.printRV[key][page]).forEach(keyPage => {
                                // key page are 1- 1-feminin ...
                                // key with feminin
                                if (keyPage.substr(-8) == "-feminin") {
                                    var keyNormal = keyPage.replace("-feminin", "");
                                    var keyFeminin = keyPage;
                                    var pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                    var pageTop = app.printRV[key][page][keyNormal]
                                    var pageBot = app.printRV[key][page][keyFeminin];

                                    if (keyPage.split("-")[1] == "verso" || keyPage.split("-")[1] == "feminin") {
                                        pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A8TopLeft");
                                    } else if (keyPage.split("-")[1] == "2") {
                                        pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A8TopRight");
                                    } else if (keyPage.split("-")[1] == "3") {
                                        pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A8BotLeft");
                                    } else if (keyPage.split("-")[1] == "4") {
                                        pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForRV", pageTop, pageBot, app.pdfWithRV, pageCount, "A8BotRight");
                                    }
                                    // key without feminin
                                } else if (!Object.keys(app.printRV[key][page]).includes(`${keyPage}-feminin`)) {
                                    var pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                    var pageSimple = app.printRV[key][page][keyPage]

                                    if (keyPage.substr(-8) != "-feminin") {
                                        if (keyPage.length == 1) {
                                            pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                            app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A8TopLeft")
                                        } else {
                                            if (keyPage.split("-")[1] == "verso") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A8TopLeft")
                                            } else if (keyPage.split("-")[1] == "2") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A8TopRight")
                                            } else if (keyPage.split("-")[1] == "3") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A8BotLeft")
                                            } else if (keyPage.split("-")[1] == "4") {
                                                pageCount = app.pdfWithRV.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForRV", pageSimple, app.pdfWithRV, pageCount, "A8BotRight")
                                            }
                                        }
                                    }
                                }
                            });
                        })
                    }
                }
            })
        }


        if (undefined != app.printSimple) {
            Object.keys(app.printSimple).forEach((key) => {
                if (Object.keys(app.printSimple[key]).length != 0) {
                    if (key == "A4") {
                        Object.keys(app.printSimple[key]).forEach((page) => {
                            // mean there is an double word on the card
                            if (Object.keys(app.printSimple[key][page]).length == 2) {
                                var pageCount = app.pdfSimple.internal.getNumberOfPages();
                                var pageTop = app.printSimple[key][page][`${Object.keys(app.printSimple[key][page])[0]}`]
                                var pageBot = app.printSimple[key][page][`${Object.keys(app.printSimple[key][page])[1]}`];
                                app.pdfSimple.addPage();
                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A4")
                            } else if (Object.keys(app.printSimple[key][page]).length == 1) {
                                var pageCount = app.pdfSimple.internal.getNumberOfPages();
                                var pageSimple = app.printSimple[key][page][`${Object.keys(app.printSimple[key][page])[0]}`]
                                app.pdfSimple.addPage();
                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A4")
                            } else {
                                console.log("error")
                            }
                        })
                    } else if (key == "A5") {
                        // mean there is an double word on the card
                        Object.keys(app.printSimple[key]).forEach((page) => {
                            // page are 1,2,3,4

                            app.pdfSimple.addPage();

                            console.log("page:", page)

                            Object.keys(app.printSimple[key][page]).forEach(keyPage => {
                                // key page are 1- 1-feminin ...
                                // key with feminin
                                if (keyPage.substr(-8) == "-feminin") {
                                    var keyNormal = keyPage.replace("-feminin", "");
                                    var keyFeminin = keyPage;
                                    var pageCount = app.pdfSimple.internal.getNumberOfPages();
                                    var pageTop = app.printSimple[key][page][keyNormal]
                                    var pageBot = app.printSimple[key][page][keyFeminin];
                                    if (Object.keys(app.printSimple[key][page]).filter((k) => k.substr(-8) == "-feminin").length == 1) {
                                        pageCount = app.pdfSimple.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A5Top")

                                    } else if (Object.keys(app.printSimple[key][page]).filter((k) => k.substr(-8) == "-feminin").length == 2) {
                                        // like 1- and 1-2     or      1- and 2- 
                                        if (Object.keys(app.printSimple[key][page])[0].substr(0, 1) == Object.keys(app.printSimple[key][page])[1].substr(0, 1)) {
                                            if (keyPage.split("-")[1] == "2") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A5Bot")
                                            } else {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A5Top")
                                            }
                                            // like  1- and 2- 
                                        } else {
                                            if (keyPage.substr(0, 1) == "1") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A5Top")
                                            } else {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A5Bot")
                                            }
                                        }
                                    }
                                    // key without feminin
                                } else if (!Object.keys(app.printSimple[key][page]).includes(`${keyPage}-feminin`)) {
                                    var pageCount = app.pdfSimple.internal.getNumberOfPages();
                                    var pageSimple = app.printSimple[key][page][keyPage]

                                    if (Object.keys(app.printSimple[key][page]).filter((k) => k.substr(-8) != "-feminin").length == 1) {
                                        pageCount = app.pdfSimple.internal.getNumberOfPages();
                                        app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A5Top")

                                    } else if (Object.keys(app.printSimple[key][page]).filter((k) => k.substr(-8) != "-feminin").length == 2) {
                                        // like 1- and 1-2
                                        if (Object.keys(app.printSimple[key][page])[0].substr(0, 1) == Object.keys(app.printSimple[key][page])[1].substr(0, 1)) {
                                            if (keyPage.split("-")[1] == "2") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A5Bot")
                                            } else {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A5Top")
                                            }

                                            // like  1- and 2- 
                                        } else {
                                            if (keyPage.substr(0, 1) == "1") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A5Top")
                                            } else {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A5Bot")
                                            }
                                        }
                                    }
                                }
                            });
                        })
                    } else if (key == "A8") {
                        Object.keys(app.printSimple[key]).forEach((page) => {
                            // page are 1,2,3,4

                            app.pdfSimple.addPage();
                            Object.keys(app.printSimple[key][page]).forEach(keyPage => {


                                // key page are 1- 1-feminin ...
                                // key with feminin
                                if (keyPage.substr(-8) == "-feminin") {
                                    var keyNormal = keyPage.replace("-feminin", "");
                                    var keyFeminin = keyPage;
                                    var pageCount = app.pdfSimple.internal.getNumberOfPages();
                                    var pageTop = app.printSimple[key][page][keyNormal]
                                    var pageBot = app.printSimple[key][page][keyFeminin];

                                    if (keyPage.split("-")[1] == "verso" || keyPage.split("-")[1] == "feminin") {
                                        pageCount = app.pdfSimple.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A8TopLeft");
                                    } else if (keyPage.split("-")[1] == "2") {
                                        pageCount = app.pdfSimple.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A8TopRight");
                                    } else if (keyPage.split("-")[1] == "3") {
                                        pageCount = app.pdfSimple.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A8BotLeft");
                                    } else if (keyPage.split("-")[1] == "4") {
                                        pageCount = app.pdfSimple.internal.getNumberOfPages();
                                        app.createCanvasToPrintDoubleCard("canvasForSimple", pageTop, pageBot, app.pdfSimple, pageCount, "A8BotRight");
                                    }
                                    // key without feminin
                                } else if (!Object.keys(app.printSimple[key][page]).includes(`${keyPage}-feminin`)) {
                                    var pageCount = app.pdfSimple.internal.getNumberOfPages();
                                    var pageSimple = app.printSimple[key][page][keyPage]

                                    if (keyPage.substr(-8) != "-feminin") {
                                        if (keyPage.length == 1) {
                                            pageCount = app.pdfSimple.internal.getNumberOfPages();
                                            app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A8TopLeft")
                                        } else {
                                            if (keyPage.split("-")[1] == "verso") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A8TopLeft")
                                            } else if (keyPage.split("-")[1] == "2") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A8TopRight")
                                            } else if (keyPage.split("-")[1] == "3") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A8BotLeft")
                                            } else if (keyPage.split("-")[1] == "4") {
                                                pageCount = app.pdfSimple.internal.getNumberOfPages();
                                                app.createCanvasToPrint("canvasForSimple", pageSimple, app.pdfSimple, pageCount, "A8BotRight")
                                            }
                                        }
                                    }
                                }
                            });
                        })
                    }
                }
            })
        }
    },

    savePDF: function () {
        var date = new Date()

        if (Object.keys(app.printRV['A4']).length != 0 || Object.keys(app.printRV['A5']).length != 0 || Object.keys(app.printRV['A8']).length != 0) {

            app.pdfWithRV.save(`PDFRectoVerso-${date.getHours()}-${date.getMinutes()}.pdf`)
        } else {
            console.log("Nothing to print in RV ")
        }
        if (Object.keys(app.printSimple['A4']).length != 0 || Object.keys(app.printSimple['A5']).length != 0 || Object.keys(app.printSimple['A8']).length != 0) {
            app.pdfSimple.save(`PDFRecto-${date.getHours()}-${date.getMinutes()}.pdf`)
        } else {
            console.log("Nothing to print in RV ")
        }
    },

    addThisCanvasToPDF(canvas, pdf, pageNumber, positionCode) {
        app.launchCanvasCreation--;
        console.log(app.launchCanvasCreation)
        if (app.launchCanvasCreation == 0) {
            $('.loading').hide();
            $('.savePDF').show()
        }
        pdf.setPage(pageNumber);
        switch (positionCode) {
            case "A4":
                pdf.addImage(canvas, 'PNG', 5, 2.5, 200, 290, '', 'NONE', 0);
                break;
            case "A5Top":
                pdf.addImage(canvas, 'PNG', 5, -200, 290 / 2, 200, '', 'NONE', -90);
                break;
            case "A5Bot":
                pdf.addImage(canvas, 'PNG', 5, -50, 290 / 2, 200, '', 'NONE', -90);
                break;
            case "A8TopLeft":
                pdf.addImage(canvas, 'PNG', 2.5, 2.5, 200 / 2, 290 / 2, '', 'NONE', 0);
                break;
            case "A8TopRight":
                pdf.addImage(canvas, 'PNG', 105, 2.5, 200 / 2, 290 / 2, '', 'NONE', 0);
                break;
            case "A8BotLeft":
                pdf.addImage(canvas, 'PNG', 2.5, 150, 200 / 2, 290 / 2, '', 'NONE', 0);
                break;
            case "A8BotRight":
                pdf.addImage(canvas, 'PNG', 105, 150, 200 / 2, 290 / 2, '', 'NONE', 0);
                break;
            default:
                break;
        }
        var canvasToClearRV = document.getElementById('canvasForRV')
        var ctxToClearRV = canvasToClearRV.getContext('2d')
        ctxToClearRV.clearRect(0, 0, canvasToClearRV.width, canvasToClearRV.height)
        var canvasToClear = document.getElementById('canvasForSimple')
        var ctxToClear = canvasToClear.getContext('2d')
        ctxToClear.clearRect(0, 0, canvasToClear.width, canvasToClear.height)
    },

    /////////////////// Utils Functions 
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    cleanList: function (className) {
        var myNode = document.getElementsByClassName(className)[0]
        if (undefined != myNode) {
            while (myNode.firstChild) {
                myNode.removeChild(myNode.firstChild);
            }
        }
    },
    // the render logic should be focusing on the rendering 
    drawGrid: function (ctx, w, h, step) {
        ctx.beginPath();
        for (var x = 0; x <= w; x += step) {
            ctx.fillText(x, x, 10);
            ctx.moveTo(x, 0);
            ctx.lineTo(x, h);
        }
        // set the color of the line
        ctx.strokeStyle = 'rgb(255,0,0)';
        ctx.lineWidth = 1;
        // the stroke will actually paint the current path 
        ctx.stroke();
        // for the sake of the example 2nd path
        ctx.beginPath();
        for (var y = 0; y <= h; y += step) {

            ctx.fillText(y, 5, y);
            ctx.moveTo(0, y);
            ctx.lineTo(w, y);
        }
        // set the color of the line
        ctx.strokeStyle = 'rgb(20,20,20)';
        // just for fun
        ctx.lineWidth = 1;
        // for your original question - you need to stroke only once
        ctx.stroke();
    },
    drawRondRect: function (rectX, rectY, rectWidth, rectHeight, cornerRadius, ctx, color) {

        // Set faux rounded corners
        ctx.lineJoin = "round";
        ctx.lineWidth = cornerRadius;
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        // Change origin and dimensions to match true size (a stroke makes the shape a bit larger)
        ctx.strokeRect(rectX + (cornerRadius / 2), rectY + (cornerRadius / 2), rectWidth - cornerRadius, rectHeight - cornerRadius);
        ctx.fillRect(rectX + (cornerRadius / 2), rectY + (cornerRadius / 2), rectWidth - cornerRadius, rectHeight - cornerRadius);
    }

};


app.init(); 